---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.prepare-function-definitions: |
    # Export functions used only by prepare

    function override {
        local TYPE="$1"
        local PACKAGE_NAME="$2"
        local PACKAGE_NAME_SANITIZED=${PACKAGE_NAME/-/_}
        local PIP_URL="${PACKAGE_NAME_SANITIZED}_pip_url"
        local RESOLVED_PIP_URL="${!PIP_URL:-}"

        if [[ -n "${RESOLVED_PIP_URL}" ]]; then
            echo_yellow "Found ${PACKAGE_NAME} override: ${RESOLVED_PIP_URL}"
            if [ "${TYPE}" = "package" ]; then
                ${VENV_PY3} -m pip uninstall -y "${PACKAGE_NAME}"
                ${VENV_PY3} -m pip install "${RESOLVED_PIP_URL}"
            else
                local BRANCH=${RESOLVED_PIP_URL##*@}
                local REPO=${RESOLVED_PIP_URL%@*}
                REPO="$(git_clean_url "${REPO}")"
                rm -rf "${SOFTWARE_DIR:?}/${PACKAGE_NAME}"
                git_clone "${REPO}" "${SOFTWARE_DIR}/${PACKAGE_NAME}"
                (
                  cd "${SOFTWARE_DIR}/${PACKAGE_NAME}"
                  loop git fetch origin "${BRANCH}"
                  git checkout FETCH_HEAD
                )
            fi
        fi
    }

    # Attempt to clone from the upstream source first. If that fails, fall back
    # to our cached clones that are updated every 15 minutes.
    # Args:
    #   $1: repository URL
    #   $2: directory for cloned repository
    #   $@: additional arguments for git clone when cloning from upstream
    function git_clone {
        local repo_url clone_dir
        repo_url=$(git_clean_url "$1")
        clone_dir=$2
        shift 2

        echo_green "Cloning ${repo_url} from upstream"
        if ! loop git clone --quiet "${repo_url}" "${clone_dir}" "$@"; then
            echo_yellow "Cloning from upstream failed, attempting to clone from cache"
            git_cache_clone "${repo_url}" "${clone_dir}"
        fi
    }

    # Run the reliable git cloner for repos on gitlab.com.
    # Args:
    #   $1: bare repo name (such as 'upt' or 'kpet')
    #   $2: directory for cloned repository
    #   $@: additional arguments for git clone
    function git_clone_gitlab_com {
        local repo_name=$1
        local clone_dir=$2
        shift 2
        git_clone "gitlab.com/cki-project/${repo_name}.git" "${clone_dir}" "$@"
    }

    function setup_software {
      echo_green "📦 Packaging CKI pipeline software."

      # Create all of the required directories.
      rm -rf "${SOFTWARE_DIR}"
      mkdir -p "${SOFTWARE_DIR}"

      # Create Python3 venv
      loop python3 -m venv "${VENV_DIR}"
      loop "${VENV_PY3}" -m pip install --upgrade "pip==21.1.*"

      # Download python packages
      for PROJECT in ${GITLAB_COM_PACKAGES}; do
          if [[ "${PROJECT}" =~ ^http ]]; then
              PROJECT="$(get_auth_git_url "${PROJECT}")"
              loop "${VENV_PY3}" -m pip install "git+${PROJECT}.git/"
          else
              loop "${VENV_PY3}" -m pip install "git+https://gitlab.com/cki-project/${PROJECT}.git/"
          fi
      done

      # Download data projects
      for PROJECT in ${GITLAB_COM_DATA_PROJECTS}; do
          if [[ "${PROJECT}" =~ ^http ]]; then
              git_clone "$(get_auth_git_url "${PROJECT}")" "${SOFTWARE_DIR}/$(basename "${PROJECT}")" --depth 1
          else
              git_clone_gitlab_com "${PROJECT}" "${SOFTWARE_DIR}/${PROJECT}" --depth 1
          fi
      done

      # CI systems for projects used in the pipeline can pass in their own versions
      # of software that they want to test.
      #
      # For each of the projects in GITLAB_COM_PACKAGES and GITLAB_COM_DATA_PROJECTS,
      # a variable of the following form can be used:
        #
      #  <project>_pip_url = https://url.com/user/project@BRANCH
      #
      # For the variable name, dashes in the project name are replaced by underscores.
      #
      # If this variable is present for a Python package, the currently installed
      # version is uninstalled and the new version is installed instead.
      #
      # If this variable is present for a data project, the currently installed
      # version is deleted and the new version is checked out instead.

      # Override Python packages if necessary.
      for PROJECT in ${GITLAB_COM_PACKAGES}; do
          override package "$(basename "${PROJECT}")"
      done

      # Override data projects if necessary.
      for PROJECT in ${GITLAB_COM_DATA_PROJECTS}; do
          override data "$(basename "${PROJECT}")"
      done

      pack_software

      ${VENV_PY3} -m pip list
    }

    function pack_software {
      echo_green "📦 Packing CKI pipeline software."
      rm -rf "${CI_PROJECT_DIR:?}/${SOFTWARE_ARTIFACTS_PATH}"
      mkdir -p "$(dirname "${CI_PROJECT_DIR}/${SOFTWARE_ARTIFACTS_PATH}")"
      tar czf "${CI_PROJECT_DIR}/${SOFTWARE_ARTIFACTS_PATH}" -C "${SOFTWARE_DIR}" .
    }


    function extract_software {
      echo_green "📦 Extracting CKI pipeline software."
      mkdir -p "${SOFTWARE_DIR}"
      tar xzf "${CI_PROJECT_DIR}/${SOFTWARE_ARTIFACTS_PATH}" -C "${SOFTWARE_DIR}"
      rm "${CI_PROJECT_DIR}/${SOFTWARE_ARTIFACTS_PATH}"
    }
