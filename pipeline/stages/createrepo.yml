---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.initialize-repo: |
  # Set the build variables
  kcidb_edit_checkout create
  kcidb_edit_checkout append-dict misc/provenance --add-environment function="executor" url="${CI_JOB_URL}"
  kcidb_edit_checkout set-bool misc/retrigger "${retrigger}"
  kcidb_edit_checkout set-bool misc/scratch "${scratch}"
  kcidb_edit_checkout set-int misc/brew_task_id "${brew_task_id:-${copr_build}}"
  [ -n "${submitter}" ] && kcidb_edit_checkout set misc/submitter "${submitter}"
  [ -n "${report_rules}" ] && kcidb_edit_checkout set misc/report_rules "${report_rules}"
  kcidb_edit_checkout set-bool misc/send_ready_for_test_pre "${send_ready_for_test_pre}"
  kcidb_edit_checkout set-bool misc/send_ready_for_test_post "${send_ready_for_test_post}"
  kcidb_edit_checkout set-json contacts "${checkout_contacts}"
  kcidb_edit_build create "${KCIDB_CHECKOUT_ID}"
  kcidb_edit_build append-dict misc/provenance --add-environment function="executor" url="${CI_JOB_URL}"
  # Save job_id of build job.
  rc_set build job_id "${CI_JOB_ID}"
  # Save build/checkout job start time.
  CREATEREPO_START_TIME="$(date_utc)"
  rc_set build start_time "${CREATEREPO_START_TIME}"
  kcidb_edit_build set-time start_time now
  rc_set checkout start_time "${CREATEREPO_START_TIME}"
  kcidb_edit_checkout set-time start_time now
  # Download prebuilt kernels and create a yum/dnf repo
  rc_state_set stage_createrepo skip
  rc_state_set package_name "${package_name}"
  kcidb_edit_build set misc/package_name "${package_name}"
  kernel_version="${nvr//"${package_name}"-/}"
  rc_state_set kernel_version "${kernel_version}"
  kcidb_edit_checkout set misc/kernel_version "${kernel_version}"
  set_kernel_architecture_debug
  if ! is_true "${skip_test}"; then
    kcidb_edit_build set-bool misc/test_plan_missing true
  fi

.download-packages: |
  # Download the packages to build the kernel
  # Change to the temporary repository directory.
  pushd "${TEMP_REPO}"
  if [ -z "${copr_build:-}" ]; then
    echo "📥 Downloading packages from Koji/Brew..."
      loop koji --server "${server_url}" \
        --weburl "${web_url}" \
        --topurl "${top_url}" \
        download-task \
        --arch="${ARCH_CONFIG}" --arch=src \
        --noprogress "${brew_task_id}"
  else
    echo "📥 Downloading packages from COPR..."
    # copr-cli uses wget to download the artifacts so we decrease
    # the verbosity to avoid the "Job's log exceeded limit of 4194304
    # bytes" message on Gitlab
    echo "verbose = off" >> ~/.wgetrc
    # Filtering by the current fedora we are tracking, name is set by
    # the trigger e.g. its value is fedora-30
    echo "accept_regex = .*(epel|${name}).*${architectures}" >> ~/.wgetrc
    # Filter out debug kernels to reduce disk usage for artifacts and
    # speed up the job. This does not delete regular 'debuginfo' packages.
    if ! is_true "${DEBUG_KERNEL}"; then
      echo "reject_regex = .*-debug-.*" >> ~/.wgetrc
    fi
    mkdir -p coprdownload
    copr-cli download-build -d coprdownload "${copr_build}"
    # Remove extra index files we couldn't filter out using regexes
    find coprdownload -name 'index.html' -exec rm {} \;
    # The tool creates a directory with chroot name which can't be overriden
    # so we need to get the builds out of there
    mv coprdownload/**/* .
    # Remove the downloaded directories. We get all chroots which we don't need
    # nor want.
    rm -R -- */
  fi
  # Restore CWD
  popd

.download-headers: |
  # Download the kernel headers and save them in their expected location
  # Change to the temporary repository directory.
  pushd "${TEMP_REPO}"
  if [[ "${package_name}" == "kernel" ]]; then
    if is_true "${download_separate_headers}"; then
      "${VENV_PY3}" -m cki.cki_tools.get_kernel_headers \
        "${ARCH}" "${brew_task_id:-${copr_build}}" "${ARTIFACTS_DIR}"
    else
      mv kernel-headers-* "${ARTIFACTS_DIR}"
    fi
  fi
  # Restore CWD
  popd

.store-artifacts: |
  # Save the artifacts in their expected location
  # Change to the temporary repository directory.
  pushd "${TEMP_REPO}"
  if is_true "${DEBUG_KERNEL}"; then
    find . -regextype posix-extended -regex ".*kernel-(rt-)*debug-.*\.rpm" \
      -exec mv '{}' "${ARTIFACTS_DIR}/" \;
    # Always ensure bpftool/selftests are copied over, even if this is a debug kernel.
    find . -regextype posix-extended -regex ".*(bpftool|selftests).*\.rpm" \
      -exec mv '{}' "${ARTIFACTS_DIR}/" \;
    # Always move over the source RPM, even if this is a debug kernel.
    find . -regextype posix-extended -regex ".*\.src\.rpm" \
      -exec mv '{}' "${ARTIFACTS_DIR}/" \;
  else
    find . -regextype posix-extended -regex ".*\.rpm" -not -regex ".*kernel-(rt-)*(debug|trace)-.*" \
      -exec mv '{}' "${ARTIFACTS_DIR}/" \;
  fi
  # Restore CWD
  popd

.createrepo:
  extends: [.with_artifacts, .with_retries, .with_python_image]
  stage: createrepo
  variables:
    ARTIFACTS: "artifacts/*.rpm artifacts/*.config"
    ARTIFACT_DEPENDENCY: |-
      prepare python
  dependencies:
    - prepare python
  before_script:
    - !reference [.common-before-script]
  script:
    - !reference [.initialize-repo]
    - |
      # Create a directory for downloading RPMs
      TEMP_REPO="${CI_PROJECT_DIR}/temp_repo"
      mkdir -p "${TEMP_REPO}"
    - !reference [.download-packages]
    - !reference [.download-headers]
    - !reference [.store-artifacts]
    - rm -rf "${TEMP_REPO}"
    - rc_state_set stage_createrepo pass
    - |
      # Retrieve and save kernel configuration
      # Some NVRs contain the .src.rpm suffix (RHEL) while others (Fedora) do not.
      # Make sure we always grab the source RPM.
      artifact_config_from_srpm "${ARTIFACTS_DIR}/${nvr%%.src.rpm}.src.rpm" "${package_name}" "${rpm_release}"
    - dump_kcidb_data "${VENV_PY3}" checkout "${KCIDB_CHECKOUT_ID}"
    - dump_kcidb_data "${VENV_PY3}" build "${KCIDB_BUILD_ID}"
    - aws_s3_upload_artifacts
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        dump_kcidb_data "${VENV_PY3}" checkout "${KCIDB_CHECKOUT_ID}"
        dump_kcidb_data "${VENV_PY3}" build "${KCIDB_BUILD_ID}"
        aws_s3_upload_artifacts
      fi
    - !reference [.common-after-script-tail]
  tags:
    - pipeline-createrepo-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_brew_koji_copr]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

createrepo i686:
  extends: [.createrepo, .i686_variables]

createrepo x86_64:
  extends: [.createrepo, .x86_64_variables]

createrepo x86_64 debug:
  extends: [.createrepo, .x86_64_variables, .debug_variables]

createrepo ppc64le:
  extends: [.createrepo, .ppc64le_variables]

createrepo aarch64:
  extends: [.createrepo, .aarch64_variables]

createrepo ppc64:
  extends: [.createrepo, .ppc64_variables]

createrepo s390x:
  extends: [.createrepo, .s390x_variables]
