#!/bin/bash
set -euo pipefail

eval "$(sed -En 's/^$|^    //p' "${BASH_SOURCE[0]%/*}"/../pipeline/functions/*.yml)"
