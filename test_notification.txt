Hello,

The CKI team is submitting your kernel build (${KERNEL_RELEASE}) for further
testing in Beaker. All the testing jobs are listed here:

  ${BEAKER_URL}/jobs/?jobsearch-0.table=Whiteboard&jobsearch-0.operation=contains&jobsearch-0.value=cki%40gitlab%3A${CI_PIPELINE_ID}

Sincerely yours,
CKI Project Team 🤖
cki-project@redhat.com
